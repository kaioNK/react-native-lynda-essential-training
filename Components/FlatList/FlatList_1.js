import React, {Component} from 'react'
import {FlatList, Text, View, Alert, StyleSheet} from 'react-native'

export default class FlatList_1 extends Component{

  renderSeparator = () => {
    return(
      <View  
        style={{height: 1, width: '100%', backgroundColor: 'blue'}}
      />
    )
  }

  //handling onPress action
  getListViewItem = (item) => {
    Alert.alert(item.key)
  }

  render(){
    return(
      <View style={styles.container}>
        <FlatList 
          data={[
            {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
            {key: 'Php'},{key: 'Hadoop'},{key: 'Sap'},  
            {key: 'Python'},{key: 'React'}, {key: 'C++'},  
            {key: 'Ruby'},{key: 'Javascript'},{key: '.Net'},  
            {key: 'Perl'},{key: 'Go'},{key: 'Angular'},  
            {key: 'Ajax'}, {key: 'React Native'},{key: 'Vue'},  
            {key: 'Rails'},{key: 'C#'},{key: 'Scala'} 
          ]}
          renderItem={({item}) => 
            <Text
              style={styles.item}
              onPress={() => this.getListViewItem(item)}
            >
              {item.key}
            </Text>
          }
          ItemSeparatorComponent={this.renderSeparator}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  },
  item:{
    padding: 10,
    fontSize: 18,
    height: 44
  }
})
