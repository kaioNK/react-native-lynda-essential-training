import React, {Component} from 'react'
import {View, Text, StyleSheet, TextInput} from 'react-native'
import PropTypes from 'prop-types';


export default class ColorForm extends Component{

  state = {
    txtColor: ''
  }

  onSubmit = () => {
    /*definindo a props 'onNewColor' que terá o valor do estado
    txtColor em lower case; esse valor será passado para o comp. pai.*/
    this.props.onNewColor(this.state.txtColor.toLowerCase())

    //apagando o valor do estado; o input ficará vazio
    this.setState({txtColor: ''})
  }

  render(){
    return(
      <View style={styles.container}>
        <TextInput 
          style={styles.txtInput} 
          placeholder='enter a color...' 
          onChangeText={(txtColor) => this.setState({txtColor})}
          value={this.state.txtColor}
        />
        <Text style={styles.button} onPress={this.onSubmit}>Add</Text>
      </View>
    )
  }
}

//Definindo que a prop onNewColor deve receber uma função e é obrigatória.
ColorForm.propTypes = {
  onNewColor: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'lightgrey',
    height: 70,
    paddingTop: 20
  },
  txtInput:{
    flex: 1,
    margin: 5,
    padding: 5,
    borderWidth: 2,
    fontSize: 20,
    borderRadius: 5,
    backgroundColor: 'snow'
  },
  button:{
    backgroundColor: 'darkblue',
    margin: 5,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 20
  }
})