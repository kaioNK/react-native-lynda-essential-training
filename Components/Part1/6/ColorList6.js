/**
 * Usando FlatList
 * Usando input data do usuário no Header. O usuário
 * irá definir a cor.
 */

import React, {Component} from 'react'
import { StyleSheet, FlatList, YellowBox } from 'react-native'
import ColorButton2 from './ColorButton2'
import ColorForm from './ColorForm'

YellowBox.ignoreWarnings(['Warning: Failed child context type'])

class ColorList6 extends Component{

  state = {
    backgroundColor: 'white',
    availableColors: [
      'blue',
      'yellow',
      'red',
    ],
  }

  onChangeColor = (backgroundColor) => {
    this.setState({backgroundColor})
  }

  renderSeparator = () => {
    return(
      <View
        style={{height: 1, width: '100%', backgroundColor: 'black'}}
      />
    )
  }

  newColor = (color) => {
    this.setState({availableColors: [...this.state.availableColors, color]})
  }


  render(){
    const {backgroundColor, availableColors} = this.state
    console.log(availableColors)
    return(
      <FlatList
        ListHeaderComponent={() => (<ColorForm onNewColor={ this.newColor} />)}
        style={[styles.container, {backgroundColor}]}
        data={availableColors}
        renderItem={({item, index}) => (
          console.log(item, index),

          <ColorButton2
            backgroundColor={item}
            onSelect={this.onChangeColor}
          />
        )}
        keyExtractor={(item, index) => index}
      />
      
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  }
})

export default ColorList6