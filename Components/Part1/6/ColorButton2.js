import React from 'react'
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native'

const ColorButton2 = ({backgroundColor, onSelect = f => f}) => (
  <TouchableHighlight style={styles.button} onPress={() => onSelect(backgroundColor)} underlayColor='#a5a5a5'>
    <View style={styles.row}>
      <View style={[styles.sample, {backgroundColor}]} />{/*é o mesmo que 'backgroundColor: backgroundColor (é o estado)', mas fica muito repetitivo, melhor usar o ES6 como está acima.*/}
      <Text style={styles.text}>{backgroundColor}</Text>
    </View>
  </TouchableHighlight >
)

const styles = StyleSheet.create({
  button:{
    margin: 10,
    padding: 10,
    borderWidth: 2,
    borderRadius: 10,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(255,255,255,.8)'
  },
  row:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  sample:{
    height: 20,
    width: 20,
    borderRadius: 10,
    margin: 5,

  },
  text:{
    fontSize: 30,
    margin: 5
  }
})

export default ColorButton2