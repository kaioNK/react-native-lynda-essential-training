/**
 * Usando FlatList
 */

import React, {Component} from 'react'
import { StyleSheet, FlatList, Text } from 'react-native'
import ColorButton2 from './ColorButton2'


class ColorList5 extends Component{

  state = {
    backgroundColor: 'white',
    availableColors: [
      'blue',
      'yellow',
      'red',
      'salmon',
      'pink',
      '#0000FF',
      'rgba(255,0,255,.9)',
      'green',
      'white',
      'black'
    ]
    
  }

  onChangeColor = (backgroundColor) => {
    this.setState({backgroundColor})
  }

  renderSeparator = () => {
    return(
      <View
        style={{height: 1, width: '100%', backgroundColor: 'black'}}
      />
    )
  }

  render(){
    const {backgroundColor, availableColors} = this.state
    console.log(availableColors)
    return(
      <FlatList
        style={[styles.container, {backgroundColor}]}
        data={availableColors}
        renderItem={({item, index}) => (
          console.log(item, index),

          <ColorButton2
            backgroundColor={item}
            onSelect={this.onChangeColor}
          />
        )}
        keyExtractor={(item, index) => index}
      />
      
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  }
})

export default ColorList5