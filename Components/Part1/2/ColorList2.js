import React, {Component} from 'react'
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native'

class ColorLis2 extends Component{

  state = {
    backgroundColor: 'blue'
  }

  changeColor = (backgroundColor) => {
    this.setState({backgroundColor})
  }

  render(){
    const {backgroundColor} = this.state
    return(
      <View style={[
        styles.container,
        {backgroundColor}
        ]}>
        <TouchableOpacity  style={styles.button} onPress={() => this.changeColor('green')}>
          <Text style={styles.text}>Green</Text>
        </TouchableOpacity >
        <TouchableOpacity  style={styles.button} onPress={() => this.changeColor('red')}>
          <Text style={styles.text}>Red</Text>
        </TouchableOpacity >
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green'
  },
  button:{
    margin: 10,
    padding: 10,
    borderWidth: 2,
    borderRadius: 10,
    alignSelf: 'stretch'
  },
  text:{
    fontSize: 30,
    textAlign: 'center'
  }
})

export default ColorLis2