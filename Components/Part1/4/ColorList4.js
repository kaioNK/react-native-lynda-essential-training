import React, {Component} from 'react'
import {ScrollView, StyleSheet } from 'react-native'
import ColorButton from './ColorButton'

class ColorList4 extends Component{

  state = {
    backgroundColor: 'white'
  }

  onChangeColor = (backgroundColor) => {
    this.setState({backgroundColor})
  }

  render(){
    const {backgroundColor} = this.state
    return(
      <ScrollView style={[styles.container, {backgroundColor}] }>
        <ColorButton backgroundColor='red' onSelect={(c) => this.onChangeColor(c)} />
        <ColorButton backgroundColor='blue' onSelect={(c) => this.onChangeColor(c)} />
        <ColorButton backgroundColor='green' onSelect={(c) => this.onChangeColor(c)} />
        <ColorButton backgroundColor='orange' onSelect={(c) => this.onChangeColor(c)} />
        <ColorButton backgroundColor='yellow' onSelect={(c) => this.onChangeColor(c)} />
        <ColorButton backgroundColor='black' onSelect={(c) => this.onChangeColor(c)} />
        <ColorButton backgroundColor='grey' onSelect={(c) => this.onChangeColor(c)} />
        <ColorButton backgroundColor='white' onSelect={(c) => this.onChangeColor(c)} />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  }
})

export default ColorList4