import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableHighlight } from 'react-native'

class ColorList3 extends Component{

  state = {
    backgroundColor: 'white'
  }

  onChangeColor = (backgroundColor) => {
    this.setState({backgroundColor})
  }

  render(){
    const {backgroundColor} = this.state
    return(
      <View style={[styles.container, {backgroundColor}] }>
        <TouchableHighlight style={styles.button} onPress={() => this.onChangeColor('yellow')} underlayColor='#a5a5a5'>
          <View style={styles.row}>
            <View style={[styles.sample, {backgroundColor: 'yellow'}]} />
            <Text style={styles.text}>Yellow</Text>
          </View>
        </TouchableHighlight >

        <TouchableHighlight style={styles.button} onPress={() => this.onChangeColor('blue')} underlayColor='#a5a5a5'>
          <View style={styles.row}>
            <View style={[styles.sample, {backgroundColor: 'blue'}]} />
            <Text style={styles.text}>Blue</Text>
          </View>
        </TouchableHighlight >

        <TouchableHighlight style={styles.button} onPress={() => this.onChangeColor('green')} underlayColor='#a5a5a5'>
          <View style={styles.row}>
            <View style={[styles.sample, {backgroundColor: 'green'}]} />
            <Text style={styles.text}>Green</Text>
          </View>
        </TouchableHighlight >

        <TouchableHighlight style={styles.button} onPress={() => this.onChangeColor('red')} underlayColor='#a5a5a5'>
          <View style={styles.row}>
            <View style={[styles.sample, {backgroundColor: 'red'}]} />
            <Text style={styles.text}>Red</Text>
          </View>
        </TouchableHighlight >

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button:{
    margin: 10,
    padding: 10,
    borderWidth: 2,
    borderRadius: 10,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(255,255,255,.8)'
  },
  row:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  sample:{
    height: 20,
    width: 20,
    borderRadius: 10,
    margin: 5,

  },
  text:{
    fontSize: 30,
    margin: 5
  }
})

export default ColorList3