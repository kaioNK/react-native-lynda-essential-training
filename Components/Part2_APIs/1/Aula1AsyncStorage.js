/**
 * Usando FlatList
 * Usando input data do usuário no Header. O usuário
 * irá definir a cor.
 * Usando AsyncStorage para armazenamento local. (Não funciona!)
 */

import React, {Component} from 'react'
import { StyleSheet, FlatList, YellowBox } from 'react-native'
import ColorButton from './ColorButton'
import ColorForm from './ColorForm'

import AsyncStorage from '@react-native-community/async-storage';

YellowBox.ignoreWarnings(['Warning: Failed child context type'])

class Aula1AsyncStorage extends Component{

  constructor(props){
    super(props)

    const availableColors = []

    this.state = {
      backgroundColor: 'white',
      availableColors,
    }
  }

  onChangeColor = (backgroundColor) => {
    this.setState({backgroundColor})
  }

  setColors = async(colors) => {
    try{
      await AsyncStorage.setItem('@ColorListStore', JSON.stringify(colors))
    }catch(err){
      console.log(err)
    }
  }

  getColors = async() => {
    try{
      const value = await AsyncStorage.getItem('@ColorListStore')
      const colors = JSON.parse(value)
      console.log('=> ', colors)
      this.setState({availableColors: colors})
      
    }catch(err){
      console.log('Erro: ', err)
    }
    console.log('Done')
  }

  getColors2 = async() => {
    const value = await AsyncStorage.getItem('@ColorListStore', (err, data) => {
      if(err){
        console.error('Error loading color ', err)
      }else{
        const availableColors = JSON.parse(data)
        this.setState({availableColors})
      }
    })
    return value
  }

  componentDidMount(){
    console.log("Olaaa")
    this.getColors()
  }

  newColor = (color) => {
    this.setState({availableColors: [...this.state.availableColors, color]})

    this.setColors(this.state.availableColors)
  }


  render(){
    const {backgroundColor, availableColors} = this.state
    console.log('availableColors: ', availableColors)
    return(
      <FlatList
        ListHeaderComponent={() => (<ColorForm onNewColor={ this.newColor} />)}
        style={[styles.container, {backgroundColor}]}
        data={availableColors}
        renderItem={({item, index}) => (
          //console.log(item, index),

          <ColorButton
            backgroundColor={item}
            onSelect={this.onChangeColor}
          />
        )}
        keyExtractor={(item, index) => index}
      />
      
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  }
})

export default Aula1AsyncStorage