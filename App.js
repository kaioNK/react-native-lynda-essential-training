import React, {Component} from 'react'
import {View, Text, StyleSheet, ImageBackground, Dimensions} from 'react-native'

import picSierra from './assets/Sierra-Spencer.png'
import picTanner from './assets/Tanner-McTab.png'

class App extends Component{
  render(){
    return(
      <View style={styles.container}>
        <ImageBackground source={picSierra} style={styles.pic}>
          <Text style={styles.userName}>Sierra Spencer</Text>
        </ImageBackground>
        <ImageBackground source={picTanner} style={styles.pic}>
          <Text style={styles.userName}>Tanner McTab</Text>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  pic:{
    flex: 1,
    width: Dimensions.get('window').width,
    resizeMode: 'cover',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  userName:{
    backgroundColor: 'rgba(0,0,0,.7)',
    fontSize: 30,
    padding: 10,
    color: 'white'
  }
})

export default App