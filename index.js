/**
 * @format
 */

import {AppRegistry} from 'react-native';
import FlatList_1 from './Components/FlatList/FlatList_1'
import ColorList3 from './Components/Part1/3/ColorList3'
import ColorList6 from './Components/Part1/6/ColorList6'
import Aula1AsyncStorage from './Components/Part2_APIs/1/Aula1AsyncStorage'

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Aula1AsyncStorage);
